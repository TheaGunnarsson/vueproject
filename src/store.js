import { createStore } from "vuex";
import {fetchCategories} from "./api/trivia";
import {fetchQuestions} from "./api/trivia";

export default createStore({
    state: {
        user: null,
        userPoints: 0,
        usersAnswers: [],

        catagories: [],

        choosenCat: "",
        choosenDiff: "",
        choosenAmount: 0,
        quizQuestions: []  
        
    },
    mutations: {
        //the useMutation hook returns an object that represents the 
        //current state of the mutation's execution
        setUser: (state, user) => {
            state.user = user
            //console.log(user)
        },
        setCategories: (state, cat) =>
        {
            state.catagories = cat;
        },
        setChoosenCat: (state, CCat) =>
        {
            state.choosenCat = CCat
        },
        setChoosenDiff: (state, diff) =>{
            state.choosenDiff = diff
        },
        setChoosenAmount: (state, amount) =>{
            state.choosenAmount = amount
        },
        setQuizQuestion: (state, quiz) => {
            state.quizQuestions = quiz
        },
        setUserPoints: (state, points) => {
            state.userPoints = points
        },
        setUsersAnswers: (state, uanswer) => {
            state.usersAnswers = uanswer
        }
    },
    actions: {
        async fetchCategories({ commit }) {
            try {
                const categories = await fetchCategories();
                commit("setCategories", categories);
            } catch (error) {
                console.log(error)
            }
        },
        async fetchQuestions({ commit, state }){
            try {
                //console.log(state.choosenAmount)
                //console.log(state.choosenCat)
                //console.log(state.choosenDiff)
                const quizQuestions = await fetchQuestions(state.choosenCat, state.choosenDiff, state.choosenAmount);
                commit("setQuizQuestion", quizQuestions);
            } catch(error){
                console.log(error)
            }
        }
    }
})