import { createRouter, createWebHistory } from "vue-router";
import  Start from "./views/Start.vue";
import Questions from "./views/Questions.vue";
import Results from "./views/Results.vue";

const routes = [
    {
        path:"/",
        component: Start
    },
    {
        path:"/Questions",
        component: Questions 
    },
    {
        path:"/Results",
        component: Results
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})