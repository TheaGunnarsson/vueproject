

const apiURL = "https://api-assignment-jt.herokuapp.com"
const apiKey = 'apiKey'

//checking if user is in api
export function ifExists(username) {
    let users={};
    fetch(`${apiURL}/trivia?username=${username}`)
    .then(response => response.json())
    .then(results => {
        if(results.length === 0){
            //Add the user
            users.user=apiPostUser(username); 
        }
        else if(results.length !== 0){
            users.user=results[0];
        }
    }) 
    return users;
}

//posting user
function apiPostUser(username) {
    let users={};
    fetch(`${apiURL}/trivia`, {
        method: 'POST',
        headers: {
            'X-API-Key': apiKey,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            username: username, 
            highScore: 0 
        })
    })
    .then(response => response.json())
    .then(newUser =>{
        users.username= newUser.username
        users.score= newUser.highScore
        users.id= newUser.id
    })
    return users; //returns 
}

//To patch score to user if it is higher than before
export async function patchScore(username, score){
    const response = await fetch(`${apiURL}/trivia?username=${username}`)
    const users = await response.json(); 
    const userId = users[0].id;
    const userScore = users[0].highScore;
    if(userScore<score){
        fetch(`${apiURL}/trivia/${userId}`, {
            method: 'PATCH', 
            headers: {
                'X-API-Key': apiKey,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                highScore: score  
            })
        })
    }else{} //hehe
}



