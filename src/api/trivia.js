

const apiURL = 'https://opentdb.com/api_category.php'
//getting all the categories from api
export async function fetchCategories() {
    try {
        const response = await fetch(apiURL);
        const categories = await response.json();
        return categories.trivia_categories;
    } catch (error) {
        console.log(error);
    }
}
//planned function that i did not have time to do
export async function fetchCatAmount(){

}
//getting the questions with users input
export async function fetchQuestions(category, difficulty, amount){
    //how do i get the questions from here?
    try{
        const response = await fetch("https://opentdb.com/api.php?amount="+ amount+"&category="+category+"&difficulty="+difficulty)
        const questions = await response.json();
        return questions.results
    } catch (error){
        console.log(error)
    }
}
